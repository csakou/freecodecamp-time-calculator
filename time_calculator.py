def add_time(start, duration, day=None):

    days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

    # Unpack values from arguments to variables
    time_list = start.split(' ')
    time_zone = time_list[1]
    start_hour, start_min = time_list[0].split(':')
    duration_hour, duration_min = duration.split(':')
    
    # Make all variables integers
    start_hour = int(start_hour)
    start_min = int(start_min)
    duration_hour = int(duration_hour)
    duration_min = int(duration_min)
    
    # Set new variables for time
    new_min = start_min + duration_min
    new_hour = start_hour

    if int(new_min) >= 60:
        new_min = new_min - 60
        new_hour = new_hour + 1
    
    days_count = 0
    days_passed = ''
    
    while ((duration_hour//24) != 0):
        duration_hour -= 24
        days_count += 1

    new_hour += duration_hour

    if int(new_hour) >= 12:
        if time_zone == "AM":
            time_zone = "PM"
        elif time_zone == "PM":
            time_zone = "AM"
            days_count += 1
        if int(new_hour) >= 13:
            new_hour -= 12

    if days_count > 1:
        days_passed = f'({days_count} days later)'
    elif days_count == 1:
        days_passed = '(next day)'

    final_str = str(new_hour) + ':' + str(new_min).zfill(2) + ' ' + time_zone

    if day != None:
        i = days.index(day.lower())
        final_str += ',' + ' ' + days[(i + days_count)%7].lower().capitalize()

    if (days_count > 0):
        final_str += ' ' + days_passed

    return final_str